import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { LoginComponent } from '../pages/login/login.component';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private authService: AuthService, private dialogRef: MatDialog) { }
  canActivate() {
    if (this.authService.IsLoggedIn()) {
      return true;
    }
    let dialogRef = this.dialogRef.open(LoginComponent);
    return false;
  }
}
