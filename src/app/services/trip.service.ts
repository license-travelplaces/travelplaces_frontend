import { TripDto } from './../models/trip-dto.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { PresentationTripDto } from '../models/presentation-trip-dto.model';
import { Trip } from '../models/trip.model';

@Injectable({
  providedIn: 'root'
})
export class TripService {

  private baseUrl = "https://localhost:44341/api/Trip/"
  private headers = new HttpHeaders({
    'Content-Type': 'application/json',
  })
  private options = { headers: this.headers, withCredentials: true }

  constructor(private http: HttpClient) { }

  createTrip(newUser: TripDto): Observable<any> {
    const body = JSON.stringify(newUser);
    return this.http.post<TripDto>(
      this.baseUrl + "Add",
      body,
      this.options
    );
  }

  getTripsByUserId(userId: number) {
    return this.http.get<PresentationTripDto[]>(
      this.baseUrl + "GetAllTripsByUser?userId=" + userId
    );
  }

  getTripByTripId(tripId: number) {
    return this.http.get<Trip>(
      this.baseUrl + "GetTripByTripId?tripId=" + tripId
    );
  }
}
