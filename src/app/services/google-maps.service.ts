import { Injectable } from '@angular/core';
import { } from 'googlemaps';
import { Observable } from 'rxjs/internal/Observable';
import { DistanceTime } from '../models/distanceTime.model';

@Injectable({
  providedIn: 'root'
})
export class GoogleMapsService {

  service: google.maps.DistanceMatrixService;

  constructor() { }

  getDistanceAndTimeBetweenTwoPoints(source: any, sourceName: string, destination: any, destinationName: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.service = new google.maps.DistanceMatrixService();
      var request2 = {
        origins: [source, sourceName],
        destinations: [destination, destinationName],
        travelMode: google.maps.TravelMode.WALKING,
      };
      var theBest: any = null;

      this.service.getDistanceMatrix(request2, (response, status) => {
        if (status == 'OK') {
          var results = response.rows[0].elements;
          for (var j = 0; j < results.length; j++) {
            var element = results[j];
            if (element.duration != undefined) {
              if (j == 0) {
                theBest = element;
              } else if (element.duration.value < theBest.duration.value) {
                theBest = element;
              }
            }
          }
        }
        resolve({
          dValue: theBest.distance.value,
          dText: theBest.distance.text,
          tValue: theBest.duration.value,
          tText: theBest.duration.text,
        });
      });
    })
  }

  async getDistanceAndTimeBetweenTwoPoints2(source: any, sourceName: string, destination: any, destinationName: string): Promise<any> {
    return await new Promise((resolve, reject) => {
      this.service = new google.maps.DistanceMatrixService();
      var request2 = {
        origins: [source, sourceName],
        destinations: [destination, destinationName],
        travelMode: google.maps.TravelMode.WALKING,
      };
      var theBest: any = null;

      this.service.getDistanceMatrix(request2, (response, status) => {
        if (status == 'OK') {
          var results = response.rows[0].elements;
          for (var j = 0; j < results.length; j++) {
            var element = results[j];
            if (element.duration != undefined) {
              if (j == 0) {
                theBest = element;
              } else if (element.duration.value < theBest.duration.value) {
                theBest = element;
              }
            }
          }
        }
        resolve({
          dValue: theBest.distance.value,
          dText: theBest.distance.text,
          tValue: theBest.duration.value,
          tText: theBest.duration.text,
        });
      });
    })
  }

}




// for (var i = 0; i < origins.length; i++) {
//   var results = response.rows[i].elements;
//   for (var j = 0; j < results.length; j++) {
//     var element = results[j];
//     var distancee = element.distance;

//     if (i == 0 && j == 0) {
//       theBest = element;
//     }
//   }
// }