import { TravelPlace } from './../models/travel-place.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { TravelPlaceImage } from '../models/travel-place-image.model';
import { TravelPlaceDto } from '../models/travel-place-dto.model';

@Injectable({
  providedIn: 'root'
})
export class TravelPlaceService {

  private baseUrl = "https://localhost:44341/api/TravelPlace/"
  private headers = new HttpHeaders({
    'Content-Type': 'application/json',
  })
  private options = { headers: this.headers, withCredentials: true }

  constructor(private http: HttpClient) { }

  getAllTravelPlacesByCityId(cityId: number): Observable<TravelPlaceDto[]> {
    return this.http.get<TravelPlaceDto[]>(
      this.baseUrl + "travelPlacesByCity?cityId=" + cityId
    );
  }

  getAllTravelPlacesByCityIdForLoggedUser(cityId: number, userId: number): Observable<TravelPlaceDto[]> {
    return this.http.get<TravelPlaceDto[]>(
      this.baseUrl + "travelPlacesByCityForLoggedUser?cityId=" + cityId + "&userId=" + userId
    );
  }

  getTravelPlaceById(travelPlaceId: number) {
    return this.http.get<TravelPlace>(
      this.baseUrl + "TravelPlaceById?travelPlaceId=" + travelPlaceId
    );
  }

  getNearByTravelPlaces(lat: number, long: number, distance: number): Observable<TravelPlaceDto[]> {
    console.log(lat);
    return this.http.get<TravelPlaceDto[]>(
      this.baseUrl + "travelPlaceNearby?latL=" + lat + "&longL=" + long + "&distance=" + distance
    );
  }

  getNearByTravelPlacesForLoggedUser(lat: number, long: number, distance: number, userId: number): Observable<TravelPlaceDto[]> {
    console.log(lat);
    return this.http.get<TravelPlaceDto[]>(
      this.baseUrl + "travelPlaceNearbyForLoggedUser?latL=" + lat + "&longL=" + long + "&distance=" + distance + "&userId=" + userId
    );
  }

  getCurentLocation(): Promise<any> {
    return new Promise((resolve, reject) => {
      navigator.geolocation.getCurrentPosition(resp => {
        resolve({ lat: 46.7749431, long: 23.6195326 });
      })
    })
  }
}
