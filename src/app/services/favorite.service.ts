import { TravelPlaceDto } from 'src/app/models/travel-place-dto.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { Favorite } from '../models/favorite.model';

@Injectable({
  providedIn: 'root'
})
export class FavoriteService {

  private baseUrl = "https://localhost:44341/api/Favorite/"
  private headers = new HttpHeaders({
    'Content-Type': 'application/json',
  })
  private options = { headers: this.headers, withCredentials: true }

  constructor(private http: HttpClient) { }

  removeFavorite(userId: number, travelPlaceId: number): Observable<any> {
    return this.http.delete(this.baseUrl + 'Delete?userId=' + userId + '&travelPlaceId=' + travelPlaceId);
  }

  addFavorite(favorite: Favorite): Observable<any> {
    const body = JSON.stringify(favorite);
    console.log(favorite)
    return this.http.post<Favorite>(
      this.baseUrl + "Add",
      body,
      this.options
    );
  }

  getfavoritesTravelPlaces(userId: number): Observable<TravelPlaceDto[]> {
    return this.http.get<TravelPlaceDto[]>(
      this.baseUrl + "FavoritesByUserId?userId=" + userId
    );
  }
}
