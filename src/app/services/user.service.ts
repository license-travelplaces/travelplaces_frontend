import { UserDto } from './../models/user-dto.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { JsonPipe } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private baseUrl = "https://localhost:44341/api/User/"
  private headers = new HttpHeaders({
    'Content-Type': 'application/json',
  })
  private options = { headers: this.headers, withCredentials: true }

  constructor(private http: HttpClient) { }

  getUserByUsernameAndPassword(user: UserDto): Observable<UserDto> {
    const body = JSON.stringify(user);
    console.log(user);
    return this.http.post<UserDto>(
      this.baseUrl + "LogIn",
      body,
      this.options
    );
  }

  createAccount(newUser: UserDto): Observable<any> {
    const body = JSON.stringify(newUser);
    console.log(newUser);
    return this.http.post<UserDto>(
      this.baseUrl + "CreateUser",
      body,
      this.options
    );
  }
}
