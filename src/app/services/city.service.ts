import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable } from 'rxjs';
import { City } from '../models/city.model';
import { CitySerch } from '../models/city-serch.model';

@Injectable({
  providedIn: 'root'
})
export class CityService {

  private baseUrl = "https://localhost:44341/api/City/"
  private headers = new HttpHeaders({
    'Content-Type': 'application/json',
  })
  private options = { headers: this.headers, withCredentials: true }

  constructor(private http: HttpClient) { }

  searchSimilarCities(input: String): Observable<CitySerch[]> {
    return this.http.get<CitySerch[]>(
      this.baseUrl + "cities?inputString=" + input
    );
  }

  getCityById(input: String): Observable<City> {
    return this.http.get<City>(
      this.baseUrl + "city?cityId=" + input
    );
  }

  getAllCities(): Observable<City[]> {
    return this.http.get<City[]>(
      this.baseUrl + "allCities"
    );
  }

  getFirst3Cities(): Observable<City[]> {
    return this.http.get<City[]>(
      this.baseUrl + "GetFirst3Cities"
    );
  }
}
