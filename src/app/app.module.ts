import { DataSharingService } from './services/data-sharing.service';
import { AuthGuard } from './services/auth.guard';
import { UserService } from './services/user.service';
import { MatIconModule } from '@angular/material/icon';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavigationBarComponent } from './shared/navigation-bar/navigation-bar.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { MessageBarComponent } from './shared/message-bar/message-bar.component';
import { CityService } from './services/city.service';
import { CityPageComponent } from './pages/city-page/city-page.component';
import { TravelPlaceService } from './services/travel-place.service';
import { MatButtonModule } from '@angular/material/button';
import { NearbyPageComponent } from './pages/nearby-page/nearby-page.component';
import { MatSliderModule } from '@angular/material/slider';
import { LoginComponent } from './pages/login/login.component';
import { MatDialogModule } from '@angular/material/dialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { MatInputModule } from '@angular/material/input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TravelPlaceComponent } from './pages/travel-place/travel-place.component';
import { AgmCoreModule } from '@agm/core';
import { FavoritesPageComponent } from './pages/favorites-page/favorites-page.component';
import { TripsPageComponent } from './pages/trips-page/trips-page.component';
import { CreateNewTripComponent } from './pages/create-new-trip/create-new-trip.component';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MessageAlertComponent } from './shared/message-alert/message-alert.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRadioModule } from '@angular/material/radio';
import { TripPageComponent } from './pages/trip-page/trip-page.component';
import { MatStepperModule } from '@angular/material/stepper';
@NgModule({
  declarations: [
    AppComponent,
    NavigationBarComponent,
    HomePageComponent,
    MessageBarComponent,
    CityPageComponent,
    NearbyPageComponent,
    LoginComponent,
    TravelPlaceComponent,
    FavoritesPageComponent,
    TripsPageComponent,
    CreateNewTripComponent,
    MessageAlertComponent,
    TripPageComponent,
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    MatIconModule,
    MatButtonModule,
    MatSliderModule,
    MatDialogModule,
    BrowserAnimationsModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
    MatAutocompleteModule,
    MatDatepickerModule,
    MatCheckboxModule,
    MatRadioModule,
    MatStepperModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBAV4nYdHlx2MBloGX1Gq-AX3XXzx9uvbU',
      libraries: ['places'],
    })
  ],
  providers: [
    CityService,
    TravelPlaceService,
    UserService,
    AuthGuard,
    DataSharingService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
