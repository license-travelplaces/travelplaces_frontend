import { map } from 'rxjs/operators';
import { CityService } from 'src/app/services/city.service';
import { TravelPlace } from './../../models/travel-place.model';
import { Trip } from './../../models/trip.model';
import { Component, Inject, OnInit } from '@angular/core';
import { GoogleMapsService } from 'src/app/services/google-maps.service';
import { TripService } from 'src/app/services/trip.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DistanceTime } from 'src/app/models/distanceTime.model';
import { ActivatedRoute } from '@angular/router';



@Component({
  selector: 'app-trip-page',
  templateUrl: './trip-page.component.html',
  styleUrls: ['./trip-page.component.css']
})
export class TripPageComponent implements OnInit {

  myMap: google.maps.Map;
  cityLocation: google.maps.LatLngLiteral;
  pointA: google.maps.LatLngLiteral;
  pointB: google.maps.LatLngLiteral;
  optionss: google.maps.MapOptions = {
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    mapTypeControl: false,
    zoom: 13
  }
  infowindowContent = document.getElementById(
    "infowindow-content"
  ) as HTMLElement;
  ds: google.maps.DirectionsService[] = [] as google.maps.DirectionsService[];
  dr: google.maps.DirectionsRenderer[] = [] as google.maps.DirectionsRenderer[];
  markersList: google.maps.Marker[] = [] as google.maps.Marker[];
  infowindows: google.maps.InfoWindow[] = [] as google.maps.InfoWindow[];

  trip: Trip = {} as Trip;
  travelPlaces: TravelPlace[] = [] as TravelPlace[];
  distanceTimesBetweenLocations: DistanceTime[] = [] as DistanceTime[];
  distanceTimeForLocation: DistanceTime = {} as DistanceTime;
  timesArrives: string[] = [] as string[];
  timesGo: string[] = [] as string[];

  constructor(private tripService: TripService,
    private cityService: CityService,
    private route: ActivatedRoute,
    private googleMapsService: GoogleMapsService, public dialogRef: MatDialogRef<TripPageComponent>,
    @Inject(MAT_DIALOG_DATA) public data: string) { }

  ngOnInit(): void {
    this.tripService.getTripByTripId(Number(this.data))
      .subscribe(async response => {
        this.trip = response;
        console.log(this.trip);
        this.sortTrvelPlaces();
        await this.setDistanceAndTimeBetweenLocations();
        this.setTimes();
        this.cityService.getCityById(this.travelPlaces[0].cityId.toString())
          .subscribe(response => {
            this.cityLocation = {
              lat: response.lat,
              lng: response.long
            };
            this.setMap();
            this.setRoutesPoliline();
          });
      });
  }

  sortTrvelPlaces(): void {
    let indexFirsEdge = this.trip.edges.findIndex(x => this.trip.edges.findIndex(y => x.startTravelPlaceId == y.endTravelPlaceId) == -1);
    let indexNextEdge = this.trip.edges.findIndex(x => this.trip.edges[indexFirsEdge].endTravelPlaceId == x.startTravelPlaceId);
    this.travelPlaces.push(this.trip.edges[indexFirsEdge].travelPlaceStart);
    this.travelPlaces.push(this.trip.edges[indexFirsEdge].travelPlaceEnd);

    while (indexNextEdge != -1) {
      this.travelPlaces.push(this.trip.edges[indexNextEdge].travelPlaceEnd);
      console.log(this.trip.edges[indexNextEdge]);
      var ala = this.trip.edges.findIndex(x => this.trip.edges[indexNextEdge].endTravelPlaceId == x.startTravelPlaceId);
      indexNextEdge = ala;
    }
  }

  async setDistanceAndTimeBetweenLocations(): Promise<void> {
    const ceva = await this.googleMapsService.getDistanceAndTimeBetweenTwoPoints2({
      lat: this.trip.latStart,
      lng: this.trip.longStart
    }, this.trip.startLocationName, {
      lat: this.travelPlaces[0].lat,
      lng: this.travelPlaces[0].long
    }, this.travelPlaces[0].name).then(resp => {
      this.distanceTimeForLocation = {
        dValue: resp.dValue,
        dText: resp.dText,
        tValue: resp.tValue,
        tText: resp.tText,
      } as DistanceTime;
      this.distanceTimesBetweenLocations.push(this.distanceTimeForLocation);
    });

    var i = 0;
    while (i < this.travelPlaces.length - 1) {
      var indexEdgeAB = this.trip.edges.findIndex(x => x.startTravelPlaceId == this.travelPlaces[i].id && x.endTravelPlaceId == this.travelPlaces[i + 1].id)
      var engeAB = this.trip.edges[indexEdgeAB];
      this.distanceTimeForLocation = {
        dValue: engeAB.dValue,
        dText: engeAB.dText,
        tValue: engeAB.tValue,
        tText: engeAB.tText,
      } as DistanceTime;
      this.distanceTimesBetweenLocations.push(this.distanceTimeForLocation);
      i++;
    }

    if (this.trip.isStartEnd) {
      await this.googleMapsService.getDistanceAndTimeBetweenTwoPoints2({
        lat: this.trip.latStart,
        lng: this.trip.longStart
      }, this.trip.startLocationName, {
        lat: this.travelPlaces[this.travelPlaces.length - 1].lat,
        lng: this.travelPlaces[this.travelPlaces.length - 1].long
      }, this.travelPlaces[this.travelPlaces.length - 1].name).then(resp => {
        this.distanceTimeForLocation = {
          dValue: resp.dValue,
          dText: resp.dText,
          tValue: resp.tValue,
          tText: resp.tText,
        } as DistanceTime;
        this.distanceTimesBetweenLocations.push(this.distanceTimeForLocation);
      });

    }


  }

  setTimes(): void {
    var timeStart = this.trip.startHour;
    var ceva = timeStart.split(':');
    var hours = Number(ceva[0]);
    var mins = Number(ceva[1]);
    var minList = this.distanceTimesBetweenLocations[0].tText.split(' ');
    var min = 0;

    if (minList.length > 2) {
      hours += Number(minList[0]);
      min = Number(minList[2])
    } else {
      min = Number(minList[0])
    }

    mins += min;
    hours += Math.floor(mins / 60);
    mins = Math.floor(mins % 60);

    this.timesArrives.push(hours.toString() + ":" + mins.toString());

    var i = 0;
    while (i < this.travelPlaces.length) {
      mins += this.travelPlaces[i].duration;
      hours += Math.floor(mins / 60);
      mins = Math.floor(mins % 60);

      this.timesGo.push(hours.toString() + ":" + mins.toString());

      if (i + 1 < this.distanceTimesBetweenLocations.length) {
        var minList = this.distanceTimesBetweenLocations[i + 1].tText.split(' ');

        if (minList.length > 2) {
          hours += Number(minList[0]);
          min = Number(minList[2])
        } else {
          min = Number(minList[0])
        }

        mins += min;
        hours += Math.floor(mins / 60);
        mins = Math.floor(mins % 60);

        this.timesArrives.push(hours.toString() + ":" + mins.toString());
      }

      i++;
    }
  }

  setMap(): void {
    this.myMap = new google.maps.Map((<HTMLInputElement>document.getElementById("map-canvas")), {
      ...this.optionss,
      center: this.cityLocation,
    });

    const card = document.getElementById("pac-card") as HTMLElement;
    //const input = document.getElementById("pac-input") as HTMLInputElement;

    const options = {
      fields: ["formatted_address", "geometry", "name"],
      strictBounds: false,
      types: ["establishment"],
    };
    this.myMap.controls[google.maps.ControlPosition.TOP_LEFT].push(card);

    const infowindow = new google.maps.InfoWindow();

    infowindow.setContent(this.infowindowContent);

    this.initMarkersAndInfoWindos();
  }

  async setRoutesPoliline(): Promise<void> {
    this.initDrAndDs();
    this.pointA = {
      lat: this.trip.latStart,
      lng: this.trip.longStart
    }
    this.pointB = {
      lat: this.travelPlaces[0].lat,
      lng: this.travelPlaces[0].long
    }
    var request = {
      origin: this.pointA,
      destination: this.pointB,
      travelMode: google.maps.TravelMode.WALKING,
    };

    await this.ds[0].route(request, (response, status) => {
      this.dr[0].setOptions({
        suppressPolylines: false,
        map: this.myMap
      });
      if (status = google.maps.DirectionsStatus.OK) {
        this.dr[0].setDirections(response);
      }
    });

    var i = 0;
    while (i < this.travelPlaces.length - 1) {
      this.pointA = {
        lat: this.travelPlaces[i].lat,
        lng: this.travelPlaces[i].long
      }
      this.pointB = {
        lat: this.travelPlaces[i + 1].lat,
        lng: this.travelPlaces[i + 1].long
      }
      var request = {
        origin: this.pointA,
        destination: this.pointB,
        travelMode: google.maps.TravelMode.WALKING,
      };

      await this.ds[i + 1].route(request, (response, status) => {
        this.dr[i + 1].setOptions({
          suppressPolylines: false,
          map: this.myMap
        });
        if (status = google.maps.DirectionsStatus.OK) {
          this.dr[i + 1].setDirections(response);
        }
      });
      i++;
    }

    if (this.trip.isStartEnd) {
      this.pointB = {
        lat: this.trip.latStart,
        lng: this.trip.longStart
      }
      this.pointA = {
        lat: this.travelPlaces[this.travelPlaces.length - 1].lat,
        lng: this.travelPlaces[this.travelPlaces.length - 1].long
      }
      var request = {
        origin: this.pointA,
        destination: this.pointB,
        travelMode: google.maps.TravelMode.WALKING,
      };

      await this.ds[this.ds.length - 1].route(request, (response, status) => {
        this.dr[this.dr.length - 1].setOptions({
          suppressPolylines: false,
          map: this.myMap
        });
        if (status = google.maps.DirectionsStatus.OK) {
          this.dr[this.dr.length - 1].setDirections(response);
        }
      });
    }
  }

  initDrAndDs(): void {
    for (var i = 0; i < this.trip.edges.length + 1; i++) {
      this.dr[i] = new google.maps.DirectionsRenderer({
        map: null,
        suppressMarkers: true
      });
      this.ds[i] = new google.maps.DirectionsService();
    }

    if (this.trip.isStartEnd) {
      this.dr.push(new google.maps.DirectionsRenderer({
        map: null,
        suppressMarkers: true
      }));
      this.ds.push(new google.maps.DirectionsService());
    }
  }

  initMarkersAndInfoWindos(): void {
    let contentString =
      '<div id="content">' +
      '<div id="siteNotice">' +
      "</div>" +
      '<h1 id="firstHeading" class="firstHeading">' + this.trip.startLocationName + '</h1>' +
      '<div id="bodyContent">' +
      "</div>" +
      "</div>";

    let point = {
      lat: this.trip.latStart,
      lng: this.trip.longStart,
    }

    this.infowindows.push(new google.maps.InfoWindow({
      content: contentString,
    }));
    this.infowindows[0].setPosition(point);

    this.markersList.push(new google.maps.Marker({
      position: point,
      icon: 'http://maps.google.com/mapfiles/kml/paddle/S.png',
      map: this.myMap,
    }));

    let mapCoppy = this.myMap;
    let coppyList = this.infowindows;
    let markerCopy = this.markersList[0];
    google.maps.event.addListener(this.markersList[0], 'click', function () {
      coppyList[0].open(mapCoppy, markerCopy);
      coppyList[0].setPosition(point);
    });

    for (let i = 0; i < this.travelPlaces.length; i++) {
      contentString =
        '<div id="content">' +
        '<div id="siteNotice">' +
        "</div>" +
        '<h1 id="firstHeading" class="firstHeading">' + this.travelPlaces[i].name + '</h1>' +
        '<div id="bodyContent">' +
        "<p>" + this.travelPlaces[i].description + "</p>" +
        '<p>See more details: <a href="http://localhost:4200/travelPlace/' + this.travelPlaces[i].id.toString() + '"> here' +
        "</p>" +
        "</div>" +
        "</div>";

      let point = {
        lat: this.travelPlaces[i].lat,
        lng: this.travelPlaces[i].long
      }

      this.infowindows.push(new google.maps.InfoWindow({
        content: contentString,
      }));

      let iconString = 'http://maps.google.com/mapfiles/kml/paddle/' + (i + 2).toString() + '.png';
      this.markersList.push(new google.maps.Marker({
        position: point,
        icon: iconString,
        map: this.myMap,
      }));

      let copyList = this.infowindows[i + 1];
      markerCopy = this.markersList[i + 1];
      google.maps.event.addListener(markerCopy, 'click', function () {
        copyList.setPosition(point),
          copyList.open(mapCoppy, markerCopy);
      });

      console.log(i);
      console.log(point);
    }
  }


}
