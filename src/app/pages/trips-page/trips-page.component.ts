import { PresentationTripDto } from './../../models/presentation-trip-dto.model';
import { TripService } from './../../services/trip.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { DataSharingService } from 'src/app/services/data-sharing.service';
import { FavoriteService } from 'src/app/services/favorite.service';
import { TravelPlaceService } from 'src/app/services/travel-place.service';
import { MatDialog } from '@angular/material/dialog';
import { CreateNewTripComponent } from '../create-new-trip/create-new-trip.component';
import { TripPageComponent } from '../trip-page/trip-page.component';

@Component({
  selector: 'app-trips-page',
  templateUrl: './trips-page.component.html',
  styleUrls: ['./trips-page.component.css']
})
export class TripsPageComponent implements OnInit {

  trips: PresentationTripDto[] = [] as PresentationTripDto[];

  constructor(private tripService: TripService,
    private route: ActivatedRoute,
    private dataSharingService: DataSharingService,
    private router: Router,
    private authService: AuthService,
    private favoriteService: FavoriteService,
    private dialogRef: MatDialog) { }

  ngOnInit(): void {
    this.dataSharingService.whiteTheme.next(false);
    this.tripService.getTripsByUserId(Number(localStorage.getItem('id')))
      .subscribe((response) => {
        this.trips = response;
        console.log(response);
      })
  }

  createTripClicked() {
    let dialogRef = this.dialogRef.open(CreateNewTripComponent, {});
  }

  openTrip(trip: PresentationTripDto) {
    var dialogRef = this.dialogRef.open(TripPageComponent, {
      data: trip.id.toString(),
    });
  }

}
