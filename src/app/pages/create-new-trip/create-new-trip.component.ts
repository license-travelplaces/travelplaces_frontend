import { TripService } from './../../services/trip.service';
import { DistanceTime } from './../../models/distanceTime.model';
import { Edge } from './../../models/edge.model';
import { TripDto } from './../../models/trip-dto.model';
import { TPTripDto } from './../../models/tp-trip-dto';
import { TravelPlace } from 'src/app/models/travel-place.model';
import { TravelPlaceService } from 'src/app/services/travel-place.service';
import { CityService } from 'src/app/services/city.service';
import { City } from './../../models/city.model';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MyErrorStateMatcher } from '../login/login.component';
import { Observable } from 'rxjs/internal/Observable';
import { map, startWith } from 'rxjs/operators';
import { TravelPlaceDto } from 'src/app/models/travel-place-dto.model';
import { MatDialog } from '@angular/material/dialog';
import { MessageAlertComponent } from 'src/app/shared/message-alert/message-alert.component';
import { trigger } from '@angular/animations';
import { GoogleMapsService } from 'src/app/services/google-maps.service';

@Component({
  selector: 'app-create-new-trip',
  templateUrl: './create-new-trip.component.html',
  styleUrls: ['./create-new-trip.component.css']
})
export class CreateNewTripComponent implements OnInit {

  myMap: google.maps.Map;
  curentLocation: google.maps.LatLngLiteral;
  location: google.maps.LatLngLiteral;
  optionss: google.maps.MapOptions = {
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    mapTypeControl: false,
    zoom: 14
  }
  infowindowContent = document.getElementById(
    "infowindow-content"
  ) as HTMLElement;

  cities: City[] = [] as City[];
  citiesCopy: City[] = [] as City[];
  travelPlaces: TravelPlaceDto[] = [] as TravelPlaceDto[];
  travelPlacesTripList: TPTripDto[] = [] as TPTripDto[];
  newTrip: TripDto = {} as TripDto;
  daysOfWeek = new Map<string, number>([["Mo", 1], ["Tu", 2], ["We", 3], ["Th", 4], ["Fr", 5], ["Sa", 6], ["Su", 7]]);
  selectedCity: City = {} as City;
  isValid = false;
  startEndRadioButtonValue: "yes" | "no";
  typeOfStartLocationRadioButtonValue: "current" | "another";

  form = new FormGroup({
    name: new FormControl('', Validators.required),
    date: new FormControl('', Validators.required),
    city: new FormControl('', Validators.required),
    startTime: new FormControl('', Validators.required),
    endTime: new FormControl('', Validators.required),
  })

  matcher = new MyErrorStateMatcher();

  constructor(private cityService: CityService,
    private travelPlaceService: TravelPlaceService,
    private tripService: TripService,
    private dialogRef: MatDialog,
    private googleMapsService: GoogleMapsService) { }

  ngOnInit(): void {
    this.selectedCity = null;
    this.cityService.getAllCities()
      .subscribe((respons) => {
        this.cities = respons;
        this.citiesCopy = respons;
      })
    this.setMap();
  }

  get name() {
    return this.form.get('name') as FormControl;
  }

  get date() {
    return this.form.get('date') as FormControl;
  }

  get city() {
    return this.form.get('city') as FormControl;
  }

  get startTime() {
    return this.form.get('startTime') as FormControl;
  }

  get endTime() {
    return this.form.get('endTime') as FormControl;
  }

  dataChange(event: any) {
    if (event.target.value == '') {
      this.selectedCity = null;
      this.cities = this.citiesCopy;
      this.travelPlaces = null;
      this.validare();
    } else {
      this.cities = this.citiesCopy.filter(x => x.name.toLocaleLowerCase().includes((event.target.value.toLocaleLowerCase())))
      this.travelPlaces = null;
    }
  }

  onCityChanged(ceva: City): void {
    this.selectedCity = ceva;
    this.validare();
    if (this.isValid) {
      this.travelPlaceService.getAllTravelPlacesByCityId(ceva.id)
        .subscribe((result) => {
          this.travelPlaces = result;
        })
    }
  }

  setTravelPlaces(): void {
    this.travelPlaceService.getAllTravelPlacesByCityId(this.selectedCity.id)
      .subscribe((result) => {
        this.travelPlaces = result;
      })
  }

  onFormChanged() {
    this.validare();
  }

  validare(): void {
    if (this.name.value != '' && this.date.value != '' && this.selectedCity != null && this.startTime.value != null && this.endTime.value != null) {
      var date1 = this.startTime.value.split(':');
      var date2 = this.endTime.value.split(':');
      if (Number(date1[0]) * 60 + Number(date1[1]) > Number(date2[0]) * 60 + Number(date2[1])) {
        this.isValid = false;
        this.dialogRef.open(MessageAlertComponent, {
          width: '350px',
          data: "The start time must be less that end time!"
        });
      } else {
        this.isValid = true;
        this.newTrip.name = this.name.value.toString();
        this.newTrip.date = this.date.value.toString();
        this.newTrip.hourStart = this.startTime.value;
        this.newTrip.hourEnd = this.endTime.value;
        this.setTravelPlaces();
      }
    } else {
      this.isValid = false;
    }
  }

  isLocationOpenValidator(program: string): TPTripDto {
    var trTripEntity = {} as TPTripDto;
    if (program == null) {
      trTripEntity.hourOpen = null;
      trTripEntity.hourClose = null;
      return trTripEntity;
    } else {
      var elems = program.split(';');
      var selectedDate = new Date(this.date.value).getDay();
      for (var i in elems) {
        var lastElem = elems[i][elems[i].length - 1];
        var days = elems[i].split('-');
        var start = days[0];
        var end = days[1][0] + days[1][1];
        if (this.daysOfWeek.get(start) <= selectedDate && this.daysOfWeek.get(end) >= selectedDate && lastElem != '-') {
          var program2 = elems[i].substring(6, elems[i].length).split('-');
          trTripEntity.hourOpen = program2[0];
          trTripEntity.hourClose = program2[1];
          return trTripEntity;
        }
      }
    }
    return null;
  }

  addOrRemovedClicked(travelPlace: TravelPlaceDto): any {
    var newLocation = this.isLocationOpenValidator(travelPlace.program);
    if (travelPlace.isFavorite == null) {
      if (newLocation != null) {
        newLocation.duration = travelPlace.duration;
        newLocation.travelPlaceId = travelPlace.id;
        newLocation.lat = travelPlace.lat;
        newLocation.long = travelPlace.long;
        newLocation.name = travelPlace.name;
        this.travelPlacesTripList.push(newLocation);
        travelPlace.isFavorite = true;
      } else {
        const dialogRef = this.dialogRef.open(MessageAlertComponent, {
          width: '350px',
          data: "Location " + travelPlace.name + " is not open on " + this.date.value,
        });
      }
    } else if (travelPlace.isFavorite == true) {
      var index = this.travelPlacesTripList.findIndex(x => x.travelPlaceId == travelPlace.id);
      this.travelPlacesTripList.splice(index, 1);
      travelPlace.isFavorite = false;
    } else if (newLocation != null && travelPlace.isFavorite == false) {
      newLocation.duration = travelPlace.duration;
      newLocation.travelPlaceId = travelPlace.id;
      newLocation.lat = travelPlace.lat;
      newLocation.long = travelPlace.long;
      newLocation.name = travelPlace.name;
      this.travelPlacesTripList.push(newLocation);
      travelPlace.isFavorite = true;
    }
  }



  setMap(): void {
    this.myMap = new google.maps.Map((<HTMLInputElement>document.getElementById("map-canvas")), {
      ...this.optionss,
      center: { lat: 40.749933, lng: -73.98633 },
    });

    const card = document.getElementById("pac-card") as HTMLElement;
    const input = document.getElementById("pac-input") as HTMLInputElement;

    const options = {
      fields: ["formatted_address", "geometry", "name"],
      strictBounds: false,
      types: ["establishment"],
    };
    this.myMap.controls[google.maps.ControlPosition.TOP_LEFT].push(card);
    const autocomplete = new google.maps.places.Autocomplete(input, options);
    autocomplete.bindTo("bounds", this.myMap);

    const infowindow = new google.maps.InfoWindow();

    infowindow.setContent(this.infowindowContent);

    const marker = new google.maps.Marker({
      map: this.myMap,
      anchorPoint: new google.maps.Point(0, -29),
    });

    autocomplete.addListener("place_changed", () => {
      infowindow.close();
      marker.setVisible(false);

      const place = autocomplete.getPlace();

      if (!place.geometry || !place.geometry.location) {
        window.alert("No details available for input: '" + place.name + "'");
        return;
      }

      if (place.geometry.viewport) {
        this.myMap.fitBounds(place.geometry.viewport);
      } else {
        this.myMap.setCenter(place.geometry.location);
        this.myMap.setZoom(17);
      }

      marker.setPosition(place.geometry.location);
      marker.setVisible(true);
      console.log("NAme");
      console.log(place);

      this.newTrip.startLocationAddress = place.formatted_address;
      this.newTrip.startLocationName = place.name;
      this.newTrip.lat = marker.getPosition().lat();
      this.newTrip.long = marker.getPosition().lng();

    });
  }

  roundTo(num: number, places: number): number {
    const factor = 10 ** places;
    return Math.round(num * factor) / factor;
  };

  async setEdgesBetweenNodes(): Promise<any> {
    var list: Edge[] = [] as Edge[];
    var l = 0;
    console.log(this.travelPlacesTripList.findIndex(x => x.name == 'Start location'));
    if (this.travelPlacesTripList.findIndex(x => x.name == 'Start location') == -1) {
      this.travelPlacesTripList.push({
        id: 0,
        travelPlaceId: null,
        name: 'Start location',
        hourOpen: null,
        hourClose: null,
        lat: this.newTrip.lat,
        long: this.newTrip.long,
        duration: 0,
      })
    }
    while (l <= this.travelPlacesTripList.length - 1) {
      this.travelPlacesTripList[l].id = l;
      l++;
    }
    let i = 0;
    let j = 0;
    while (i != this.travelPlacesTripList.length) {
      j = i + 1;
      while (j != this.travelPlacesTripList.length) {
        if (i != j) {
          const ceva = await this.googleMapsService.getDistanceAndTimeBetweenTwoPoints2({
            lat: this.travelPlacesTripList[i].lat,
            lng: this.travelPlacesTripList[i].long
          }, this.travelPlacesTripList[i].name, {
            lat: this.travelPlacesTripList[j].lat,
            lng: this.travelPlacesTripList[j].long
          }, this.travelPlacesTripList[j].name).then(resp => {
            list.push({
              nodeA: this.travelPlacesTripList[i].id,
              nodeB: this.travelPlacesTripList[j].id,
              tText: resp.tText,
              tValue: resp.tValue,
              dText: resp.dText,
              dValue: resp.dValue,
              distance: this.roundTo((resp.tValue / 60), 2)
            } as Edge);
          });
        }
        j++;
      }
      i++;
    }
    this.newTrip.edges = list;
  }

  async submit(): Promise<void> {
    console.log(this.travelPlacesTripList);
    await this.setEdgesBetweenNodes();
    if (this.newTrip.lat == null && this.newTrip.long == null) {
      var lala = this.dialogRef.open(MessageAlertComponent, {
        width: '350px',
        data: "You must select the start location!",
      });
    }
    this.newTrip.travelPlacesTrip = this.travelPlacesTripList;
    this.newTrip.userId = Number(localStorage.getItem('id'));
    if (this.startEndRadioButtonValue.toString() == "yes") {
      this.newTrip.isStartEnd = true;
    } else
      this.newTrip.isStartEnd = false;
    console.log(this.newTrip);
    this.tripService.createTrip(this.newTrip).subscribe((resp) => {
      if (resp) {
        this.dialogRef.open(MessageAlertComponent, {
          width: '350px',
          data: "The trip was added in your list!",
        });
      } else {
        this.dialogRef.open(MessageAlertComponent, {
          width: '350px',
          data: "Try to changed start hour, end hour or remove some locations!",
        });
      }
    }
    );
  }
}
