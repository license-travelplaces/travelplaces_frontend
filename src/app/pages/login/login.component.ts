import { UserDto } from './../../models/user-dto.model';
import { UserService } from './../../services/user.service';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { UserType } from 'src/app/enums/user-type.enum';
import { DataSharingService } from 'src/app/services/data-sharing.service';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  isCreateAccountClicked = false;
  isLogInCreateClicked = true;

  constructor(private userService: UserService, private dataSharingService: DataSharingService) { }

  ngOnInit(): void {
  }

  usernameFormControl = new FormControl('', [Validators.required, Validators.email]);

  logInForm = new FormGroup({
    username: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
  })

  get username() {
    return this.logInForm.get('username') as FormControl;
  }

  get password() {
    return this.logInForm.get('password') as FormControl;
  }

  registerForm = new FormGroup({
    usernameR: new FormControl('', Validators.required),
    email: new FormControl('', [Validators.required, Validators.email]),
    passwordR: new FormControl('', Validators.required),
    validationPaddword: new FormControl('', Validators.required),
  })

  get usernameR() {
    return this.registerForm.get('usernameR') as FormControl;
  }

  get passwordR() {
    return this.registerForm.get('usernameR') as FormControl;
  }

  get validationPaddword() {
    return this.registerForm.get('validationPaddword') as FormControl;
  }

  get email() {
    return this.registerForm.get('email') as FormControl;
  }

  matcher = new MyErrorStateMatcher();

  value = 'Clear me';

  onStrengthChanged(strength: number) {
    console.log('password strength = ', strength);
  }

  logInButtonClicked(): void {
    var user = {
      id: 0,
      username: this.username.value,
      password: this.password.value,
      email: '',
      userType: UserType.NormalUser,
    } as UserDto;
    this.userService
      .getUserByUsernameAndPassword(user)
      .subscribe((newUser) => {
        localStorage.setItem('username', newUser.username);
        localStorage.setItem('id', newUser.id.toString());
        this.dataSharingService.isUserLoggedIn.next(true);
        window.location.reload();
      })
  }

  registerButtonClicked(): void {
    var error = "";
    if (this.usernameR.value.lenght() < 8) {
      error += "Username invalid!\n";
    }
    if (this.email.value.lenght() == 0) {
      error += "E-mail invalid!\n";
    }
    if (this.passwordR.value.lenght() < 8) {
      error += "Password invalid!\n";
    }
    if (this.passwordR.value == this.validationPaddword.value) {
      error += "Password is not same as Validation Password!\n";
    }
    if (error != "") {
      var user = {
        id: 0,
        username: this.usernameR.value,
        password: this.passwordR.value,
        email: this.email.value,
        userType: UserType.NormalUser,
      } as UserDto;
      this.userService
        .createAccount(user)
        .subscribe(() => {
        })
    } else {
      //error
    }
  }

}
