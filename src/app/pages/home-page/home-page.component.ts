import { City } from './../../models/city.model';
import { CityService } from './../../services/city.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MessageBarComponent } from 'src/app/shared/message-bar/message-bar.component';
import { HttpResponse } from '@angular/common/http';
import { CitySerch } from 'src/app/models/city-serch.model';
import { Router } from '@angular/router';
import { DataSharingService } from 'src/app/services/data-sharing.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {

  similarCities: CitySerch[] = [] as CitySerch[];
  cities: City[] = [] as City[];
  hasQuery: Boolean = false;
  typeButton = '';
  fromErrors: string[] = [];
  fromSuccesses: string[] = [];
  @ViewChild('messageBar') messageBar = {} as MessageBarComponent;
  isWhieteThem = false;

  constructor(private cityService: CityService, private router: Router, private dataSharingService: DataSharingService) { }

  ngOnInit(): void {
    this.dataSharingService.whiteTheme.next(true);
    this.cityService.getFirst3Cities()
      .subscribe((resp) => {
        this.cities = resp;
      })
  }

  onRadioChange(type: string): void {
    if (type == 'Location') {
      this.typeButton = 'Location';
    } else if (type == 'City') {
      this.typeButton = 'City';
    } else {
      this.typeButton = 'Country';
    }
    console.log(this.typeButton);
  }

  resultClicked(cityId: number) {
    this.router.navigate(['/city/' + cityId]);
  }

  nearByClicked() {
    this.router.navigate(['/nearby']);
  }

  sendData(event: any) {
    if (event.target.value == '') {
      this.hasQuery = false;
      this.similarCities = [];
    } else {
      this.cityService
        .searchSimilarCities(event.target.value)
        .subscribe((cities) => {
          this.similarCities = cities;
          this.hasQuery = true;
        },
          (error) => {

          }
        );
    }
  }
}
