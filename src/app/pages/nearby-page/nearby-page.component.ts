import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { Favorite } from 'src/app/models/favorite.model';
import { TravelPlaceDto } from 'src/app/models/travel-place-dto.model';
import { AuthService } from 'src/app/services/auth.service';
import { CityService } from 'src/app/services/city.service';
import { DataSharingService } from 'src/app/services/data-sharing.service';
import { FavoriteService } from 'src/app/services/favorite.service';
import { TravelPlaceService } from 'src/app/services/travel-place.service';
import { LoginComponent } from '../login/login.component';

@Component({
  selector: 'app-nearby-page',
  templateUrl: './nearby-page.component.html',
  styleUrls: ['./nearby-page.component.css']
})
export class NearbyPageComponent implements OnInit {

  travelPlacesByCity: TravelPlaceDto[] = [] as TravelPlaceDto[];
  travelPlacesByCityCopy: TravelPlaceDto[] = [] as TravelPlaceDto[];
  distance = 20;
  curentLat: number = 0;
  curentLong: number = 0;

  constructor(private travelPlaceService: TravelPlaceService, private route: ActivatedRoute, private dataSharingService: DataSharingService,
    private authService: AuthService,
    private favoriteService: FavoriteService,
    private dialogRef: MatDialog) { }


  formatLabel(value: number) {
    return value + 'km';
  }

  ngOnInit(): void {
    this.dataSharingService.whiteTheme.next(true);
    this.travelPlaceService.getCurentLocation().then(resp => {
      this.curentLat = resp.lat;
      this.curentLong = resp.long;

      if (this.authService.IsLoggedIn()) {
        this.travelPlaceService
          .getNearByTravelPlacesForLoggedUser(this.curentLat, this.curentLong, this.distance, Number(localStorage.getItem('id')))
          .subscribe((cities) => {
            this.travelPlacesByCity = cities;
            console.log(this.travelPlacesByCity);
          },
            (error) => { }
          );
      } else {
        this.travelPlaceService
          .getNearByTravelPlaces(this.curentLat, this.curentLong, this.distance)
          .subscribe((cities) => {
            this.travelPlacesByCity = cities;
            console.log(this.travelPlacesByCity);
          },
            (error) => { }
          );
      }
    });
  }

  updateValue(event: any) {
    if (event.value != 0) {
      this.travelPlaceService
        .getNearByTravelPlaces(this.curentLat, this.curentLong, event.value)
        .subscribe((cities) => {
          this.travelPlacesByCity = cities;
          console.log(this.travelPlacesByCity);
        },
          (error) => {

          }
        );
    } else {
      this.travelPlaceService
        .getNearByTravelPlaces(this.curentLat, this.curentLong, this.distance)
        .subscribe((cities) => {
          this.travelPlacesByCity = cities;
          console.log(this.travelPlacesByCity);
        },
          (error) => {

          }
        );
    }
  }

  favoriteClicked(target: TravelPlaceDto) {
    console.log(target);
    if (!this.authService.IsLoggedIn()) {
      let dialogRef = this.dialogRef.open(LoginComponent, {});
    } else {
      console.log(target);
      var favorite = {
        userId: Number(localStorage.getItem('id')),
        travelPlaceId: target.id
      } as Favorite;
      if (target.isFavorite) {
        target.isFavorite = false;
        this.favoriteService.removeFavorite(Number(localStorage.getItem('id')), target.id).subscribe((ceva) => {
        });
      } else {
        target.isFavorite = true;
        this.favoriteService.addFavorite(favorite).subscribe((ceva) => {
        });
      }
    }
  }
}
