import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NearbyPageComponent } from './nearby-page.component';

describe('NearbyPageComponent', () => {
  let component: NearbyPageComponent;
  let fixture: ComponentFixture<NearbyPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NearbyPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NearbyPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
