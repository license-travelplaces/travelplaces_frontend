import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CitySerch } from 'src/app/models/city-serch.model';
import { Favorite } from 'src/app/models/favorite.model';
import { TravelPlaceDto } from 'src/app/models/travel-place-dto.model';
import { TravelPlaceImage } from 'src/app/models/travel-place-image.model';
import { AuthService } from 'src/app/services/auth.service';
import { DataSharingService } from 'src/app/services/data-sharing.service';
import { FavoriteService } from 'src/app/services/favorite.service';
import { TravelPlaceService } from 'src/app/services/travel-place.service';

@Component({
  selector: 'app-favorites-page',
  templateUrl: './favorites-page.component.html',
  styleUrls: ['./favorites-page.component.css']
})
export class FavoritesPageComponent implements OnInit {

  travelPlaces: TravelPlaceDto[] = [] as TravelPlaceDto[];
  travelPlacesCoppy: TravelPlaceDto[] = [] as TravelPlaceDto[];
  image: TravelPlaceImage = {} as TravelPlaceImage;
  sanitizedImageData: any;
  imageData: any;
  similarCities: CitySerch[] = [] as CitySerch[];
  hasQuery: Boolean = false;

  constructor(private travelPlaceService: TravelPlaceService,
    private route: ActivatedRoute,
    private dataSharingService: DataSharingService,
    private router: Router,
    private authService: AuthService,
    private favoriteService: FavoriteService) { }

  ngOnInit(): void {
    this.dataSharingService.whiteTheme.next(false);
    this.favoriteService
      .getfavoritesTravelPlaces(Number(localStorage.getItem('id')))
      .subscribe((travelPlaces) => {
        this.travelPlaces = travelPlaces;
        this.travelPlacesCoppy = travelPlaces;
      })
  }

  favoriteClicked(target: TravelPlaceDto) {
    target.isFavorite = false;
    this.favoriteService.removeFavorite(Number(localStorage.getItem('id')), target.id).subscribe((ceva) => { })
    this.travelPlaces = this.travelPlaces.filter(x => x.isFavorite == true);
    this.travelPlacesCoppy = this.travelPlacesCoppy.filter(x => x.isFavorite == true);
  }

  moreDetailsClicked(travelPlaceId: number) {
    this.router.navigate(['/travelPlace/' + travelPlaceId]);
  }

  dataChange(event: any) {
    var ceva = event.target.value.toString();
    if (event.target.value == '') {
      this.travelPlaces = this.travelPlacesCoppy;
    } else {
      this.travelPlaces = this.travelPlacesCoppy.filter(x => x.name.toLocaleLowerCase().includes((event.target.value.toLocaleLowerCase())))
    }
  }
}
