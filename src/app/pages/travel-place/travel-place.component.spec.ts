import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TravelPlaceComponent } from './travel-place.component';

describe('TravelPlaceComponent', () => {
  let component: TravelPlaceComponent;
  let fixture: ComponentFixture<TravelPlaceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TravelPlaceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TravelPlaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
