import { DistanceTime } from './../../models/distanceTime.model';
import { GoogleMapsService } from './../../services/google-maps.service';
import { TravelPlaceService } from './../../services/travel-place.service';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataSharingService } from 'src/app/services/data-sharing.service';
import { TravelPlace } from 'src/app/models/travel-place.model';
import { } from 'googlemaps';


@Component({
  selector: 'app-travel-place',
  templateUrl: './travel-place.component.html',
  styleUrls: ['./travel-place.component.css']
})
export class TravelPlaceComponent implements OnInit {


  myMap: google.maps.Map;
  curentLocation: google.maps.LatLngLiteral;
  location: google.maps.LatLngLiteral;
  options: google.maps.MapOptions = {
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    scrollwheel: true,
    disableDefaultUI: true,
    disableDoubleClickZoom: true,
    zoom: 14
  }

  ds: google.maps.DirectionsService;
  dr: google.maps.DirectionsRenderer;

  travePlaceEntity: TravelPlace = {} as TravelPlace;
  value = '';
  cityName = '';
  countryName = '';
  distanceTimeForLocation: DistanceTime = {} as DistanceTime;

  constructor(private travePlaceService: TravelPlaceService,
    private route: ActivatedRoute,
    private dataSharingService: DataSharingService,
    private googleMapsService: GoogleMapsService,
    private router: Router) { }

  ngOnInit(): void {
    this.dataSharingService.whiteTheme.next(false);

    this.ds = new google.maps.DirectionsService();
    this.dr = new google.maps.DirectionsRenderer({
      map: null,
      suppressMarkers: true
    });
    this.travePlaceService
      .getTravelPlaceById(this.route.snapshot.params['id'])
      .subscribe((travelPlace) => {

        this.travePlaceEntity = travelPlace;
        if (travelPlace.city != null) {
          this.cityName = travelPlace.city?.name;
        }
        this.location = {
          lat: travelPlace.lat,
          lng: travelPlace.long
        }
        if (travelPlace.images != null) {
          this.value = travelPlace.images[0].imageUrl;
        } else {
          this.value = 'https://www.industrialempathy.com/img/remote/ZiClJf-1920w.jpg';
        }

        this.travePlaceService.getCurentLocation().then(resp => {
          this.curentLocation = {
            lat: resp.lat,
            lng: resp.long
          }
          this.setMap();
          this.setRoutePoliline();
          this.googleMapsService.getDistanceAndTimeBetweenTwoPoints(this.curentLocation, 'Curent Location', this.location, this.travePlaceEntity.name).then(resp => {
            this.distanceTimeForLocation = {
              dValue: resp.dValue,
              dText: resp.dText,
              tValue: resp.tValue,
              tText: resp.tText,
            } as DistanceTime;
          });

        });
      },
        (error) => {

        }
      );
  }

  setMap(): void {
    this.myMap = new google.maps.Map((<HTMLInputElement>document.getElementById("map-canvas")), {
      ...this.options,
      center: this.curentLocation
    });

    var markerCurentLocation = new google.maps.Marker({
      position: this.curentLocation,
      map: this.myMap
    })

    var markerLocation = new google.maps.Marker({
      position: this.location,
      map: this.myMap
    })
  }

  setRoutePoliline(): void {
    var request = {
      origin: this.curentLocation,
      destination: this.location,
      travelMode: google.maps.TravelMode.WALKING,
    };

    this.ds.route(request, (response, status) => {
      this.dr.setOptions({
        suppressPolylines: false,
        map: this.myMap
      });
      if (status = google.maps.DirectionsStatus.OK) {
        this.dr.setDirections(response);
      }
    })
  }
}