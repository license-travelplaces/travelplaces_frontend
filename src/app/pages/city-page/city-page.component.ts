import { Favorite } from './../../models/favorite.model';
import { FavoriteService } from './../../services/favorite.service';
import { AuthService } from './../../services/auth.service';
import { City } from './../../models/city.model';
import { TravelPlaceImage } from './../../models/travel-place-image.model';
import { TravelPlaceService } from './../../services/travel-place.service';
import { TravelPlace } from './../../models/travel-place.model';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TravelPlaceDto } from 'src/app/models/travel-place-dto.model';
import { CityService } from 'src/app/services/city.service';
import { MatButtonModule } from '@angular/material/button';
import { DataSharingService } from 'src/app/services/data-sharing.service';
import { MatDialog } from '@angular/material/dialog';
import { LoginComponent } from '../login/login.component';

@Component({
  selector: 'app-city-page',
  templateUrl: './city-page.component.html',
  styleUrls: ['./city-page.component.css']
})
export class CityPageComponent implements OnInit {

  travelPlacesByCity: TravelPlaceDto[] = [] as TravelPlaceDto[];
  travelPlacesByCityCoppy: TravelPlaceDto[] = [] as TravelPlaceDto[];
  image: TravelPlaceImage = {} as TravelPlaceImage;
  city: City = {} as City;
  cityId = 0;
  sanitizedImageData: any;
  imageData: any;

  constructor(private travelPlaceService: TravelPlaceService,
    private cityService: CityService,
    private route: ActivatedRoute,
    private dataSharingService: DataSharingService,
    private router: Router,
    private authService: AuthService,
    private favoriteService: FavoriteService,
    private dialogRef: MatDialog) { }

  ngOnInit(): void {
    this.dataSharingService.whiteTheme.next(true);

    this.cityService
      .getCityById(this.route.snapshot.params['id'])
      .subscribe((city) => {
        this.city = city;
      },
        (error) => {

        }
      );

    if (this.authService.IsLoggedIn()) {
      this.travelPlaceService
        .getAllTravelPlacesByCityIdForLoggedUser(this.route.snapshot.params['id'], Number(localStorage.getItem('id')))
        .subscribe((cities) => {
          console.log(cities);
          this.travelPlacesByCity = cities;
          this.travelPlacesByCityCoppy = cities;
        },
          (error) => {

          }
        );
    } else {
      this.travelPlaceService
        .getAllTravelPlacesByCityId(this.route.snapshot.params['id'])
        .subscribe((cities) => {
          this.travelPlacesByCity = cities;
          this.travelPlacesByCityCoppy = cities;
        },
          (error) => {

          }
        );
    }
  }

  dataChange(event: any) {
    var ceva = event.target.value.toString();
    if (event.target.value == '') {
      this.travelPlacesByCity = this.travelPlacesByCityCoppy;
    } else {
      this.travelPlacesByCity = this.travelPlacesByCityCoppy.filter(x => x.name.toLocaleLowerCase().includes((event.target.value.toLocaleLowerCase())))
    }
  }

  moreDetailsClicked(travelPlaceId: number) {
    this.router.navigate(['/travelPlace/' + travelPlaceId]);
  }

  favoriteClicked(target: TravelPlaceDto) {
    console.log(target);
    if (!this.authService.IsLoggedIn()) {
      let dialogRef = this.dialogRef.open(LoginComponent, {});
    } else {
      console.log(target);
      var favorite = {
        userId: Number(localStorage.getItem('id')),
        travelPlaceId: target.id
      } as Favorite;
      if (target.isFavorite) {
        target.isFavorite = false;
        this.favoriteService.removeFavorite(Number(localStorage.getItem('id')), target.id).subscribe((ceva) => {

        });
      } else {
        target.isFavorite = true;
        this.favoriteService.addFavorite(favorite).subscribe((ceva) => {

        });
      }
    }
  }

}
