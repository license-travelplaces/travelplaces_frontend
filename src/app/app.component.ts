import { LoginComponent } from './pages/login/login.component';
import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'travelPlacesFE';

  constructor(private dialogRef: MatDialog) { }

  openDialog() {
    this.dialogRef.open(LoginComponent);
  }
}
