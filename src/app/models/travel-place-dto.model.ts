export interface TravelPlaceDto {
    id: number;
    name: string;
    description: string;
    program: string;
    duration: number;
    imageUrl?: string;
    cityName?: string;
    distance?: number;
    isFavorite?: boolean;
    lat?: number;
    long?: number;
}