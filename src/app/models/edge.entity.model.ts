import { Trip } from './trip.model';
import { TravelPlace } from './travel-place.model';
export interface EdgeEntity {
    tripId?: number
    startTravelPlaceId: number,
    endTravelPlaceId: number,
    travelPlaceStart: TravelPlace,
    travelPlaceEnd: TravelPlace,
    trip?: Trip,
    dValue: number,
    dText: string,
    tValue: number,
    tText: string,
}