export interface CitySerch {
    id: number;
    name: string;
    countryName: string;
}