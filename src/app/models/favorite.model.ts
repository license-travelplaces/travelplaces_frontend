export interface Favorite {
    userId: number,
    travelPlaceId: number,
}