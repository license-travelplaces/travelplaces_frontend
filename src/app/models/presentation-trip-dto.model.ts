export interface PresentationTripDto {
    id: number;
    name: string;
    duration: string;
    numberLocations: number;
    imagesUrl: Array<string>;
}