export interface Edge {
    nodeA: number,
    nodeB: number,
    distance: number,
    dValue: number,
    dText: string,
    tValue: number,
    tText: string,
}