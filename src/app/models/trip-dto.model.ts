import { TPTripDto } from './tp-trip-dto';
import { Data } from '@angular/router';
import { City } from './city.model';
import { Edge } from './edge.model';
export interface TripDto {
    userId: number,
    name: string,
    date: Data,
    city: City,
    hourStart?: string,
    hourEnd?: string,
    lat: number,
    long: number,
    startLocationAddress?: string,
    startLocationName?: string,
    isStartEnd: boolean,
    travelPlacesTrip: Array<TPTripDto>,
    edges: Array<Edge>,
}