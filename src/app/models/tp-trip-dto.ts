export interface TPTripDto {
    id?: number,
    travelPlaceId?: number,
    name: string,
    hourOpen?: string,
    hourClose?: string,
    lat: number,
    long: number,
    duration: number,
}