import { Country } from "./country.model";

export interface City {
    id: number;
    name: string;
    lat?: number;
    long?: number;
    imageUrl: string;
    countryId: number;
    country: Country;
}