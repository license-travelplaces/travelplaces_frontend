import { City } from './city.model';
import { TravelPlaceImage } from './travel-place-image.model';
export interface TravelPlace {
    id: number;
    name: string;
    description: string;
    lat: number;
    long: number;
    program: string;
    duration: number;
    cityId: number
    city?: City;
    images?: Array<TravelPlaceImage>
}