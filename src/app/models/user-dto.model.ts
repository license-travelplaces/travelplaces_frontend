import { UserType } from './../enums/user-type.enum';
export interface UserDto {
    id: number;
    username: string;
    password: string;
    email?: string;
    userType?: UserType;
}