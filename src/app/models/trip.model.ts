import { Data } from "@angular/router";
import { EdgeEntity } from "./edge.entity.model";

export interface Trip {
    id: number,
    name: string,
    latStart: number,
    longStart: number,
    startLocationAddress?: string,
    startLocationName?: string,
    dateStart: Date,
    startHour?: string,
    endHour?: string,
    isStartEnd: boolean,
    duration: number,
    userId: number,
    edges: Array<EdgeEntity>,
}