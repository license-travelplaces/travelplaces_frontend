import { Byte } from "@angular/compiler/src/util";

export interface TravelPlaceImage {
    id: number;
    name: string;
    imageUrl: string;
    travelPlaceId?: number;
}