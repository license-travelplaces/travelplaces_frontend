export interface DistanceTime {
    dValue: number,
    dText: string,
    tValue: number,
    tText: string,
}