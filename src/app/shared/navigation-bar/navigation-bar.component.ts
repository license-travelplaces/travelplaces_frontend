import { LoginComponent } from 'src/app/pages/login/login.component';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AuthService } from 'src/app/services/auth.service';
import { DataSharingService } from 'src/app/services/data-sharing.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navigation-bar',
  templateUrl: './navigation-bar.component.html',
  styleUrls: ['./navigation-bar.component.css']
})
export class NavigationBarComponent implements OnInit {

  isLogged: Boolean = false;
  logInClicked: Boolean = false;
  isWhileTheme: Boolean = false;

  constructor(private dialogRef: MatDialog, private dataSharingService: DataSharingService,
    private authService: AuthService,
    private router: Router) {
    this.dataSharingService.isUserLoggedIn.subscribe(value => {
      this.isLogged = value;
    });
    this.dataSharingService.whiteTheme.subscribe(value => {
      this.isWhileTheme = value;
    });
  }

  ngOnInit(): void {
  }

  longInClicked(): void {
    this.logInClicked = true;
  }

  openDialog() {
    let dialogRef = this.dialogRef.open(LoginComponent, {});
  }

  isUserLogged() {
    return this.authService.IsLoggedIn();
  }

  logOut() {
    if (this.authService.IsLoggedIn()) {
      localStorage.removeItem('username');
      localStorage.removeItem('id');
      window.location.reload();
    } else {
      this.openDialog();
    }
  }

  favoritesClicked(): void {
    if (this.authService.IsLoggedIn()) {
      this.router.navigate(['/favorites']);
    } else {
      this.openDialog();
    }
  }

  tripsClicked() {
    if (this.authService.IsLoggedIn()) {
      this.router.navigate(['/trips']);
    } else {
      this.openDialog();
    }
  }

  logoClicked(): void {
    //if (this.authService.IsLoggedIn()) {
    this.router.navigate(['']);
    //} else {
    //  this.openDialog();
    //}
  }

}
