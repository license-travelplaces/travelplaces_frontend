import { TripsPageComponent } from './pages/trips-page/trips-page.component';
import { TravelPlaceComponent } from './pages/travel-place/travel-place.component';
import { AuthService } from './services/auth.service';
import { NearbyPageComponent } from './pages/nearby-page/nearby-page.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CityPageComponent } from './pages/city-page/city-page.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { AuthGuard } from './services/auth.guard';
import { FavoritesPageComponent } from './pages/favorites-page/favorites-page.component';

const routes: Routes = [
  { path: '', component: HomePageComponent },
  { path: 'city/:id', component: CityPageComponent },
  { path: 'nearby', component: NearbyPageComponent },
  { path: 'travelPlace/:id', component: TravelPlaceComponent },
  { path: 'favorites', component: FavoritesPageComponent, canActivate: [AuthGuard] },
  { path: 'trips', component: TripsPageComponent, canActivate: [AuthGuard] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
